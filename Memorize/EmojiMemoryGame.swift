//
//  EmojiMemoryGame.swift
//  Memorize
//
//  Created by Sergio Escalante Ordonez on 14/7/21.
//

import SwiftUI

class EmojiMemoryGame: ObservableObject {
    
    typealias Card = MemoryGame<String>.Card
    
    private static let emojis = ["🍏", "🍇", "🍍", "🥬", "🧄", "🥖", "🍓", "🥥", "🫐", "🥝", "🍅", "🥑", "🍕", "🫑", "🍑", "🍆", "🍥", "🍡", "🥩", "🍙"]
    
    private static func createMemoryGame() -> MemoryGame<String> {
        MemoryGame<String>(numberOfPairsOfCards: emojis.count) { pairIndex in emojis[pairIndex] }
    }
    
    @Published private var model = createMemoryGame()
    
    var cards: Array<Card> {
        return model.cards
    }
    
    // MARK: - Intent(s)
    
    func choose(_ card: Card) {
        model.choose(card)
    }
}

struct EmojiMemoryGame_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
