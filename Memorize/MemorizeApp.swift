//
//  MemorizeApp.swift
//  Memorize
//
//  Created by Sergio Escalante Ordonez on 11/7/21.
//

import SwiftUI

@main
struct MemorizeApp: App {
    
    private let game = EmojiMemoryGame()
    
    var body: some Scene {
        WindowGroup {
            EmojiMemoryGameView(game: game)
        }
    }
}
